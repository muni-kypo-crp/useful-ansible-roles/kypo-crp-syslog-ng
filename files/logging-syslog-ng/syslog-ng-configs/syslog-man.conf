options {
    keep-hostname(yes);
};

# MAN Logs Source
source s_man {
    network(
       ip(0.0.0.0)
       port(515)
       transport("tcp")
       flags(syslog-protocol)
       #Maximum clients, that can be served by syslog-ng
       max-connections(300)
       log_iw_size(30000)
    );
};

# BASH and MSF4 commands common settings
template kypo_man_console_command_syslog_template {
    template("{\"event\":$(format-json --pair hostname=$HOST --pair ip=${.SDATA.kypo.fromhost_ip} --pair fromhost_ip=$SOURCEIP --pair programname=$PROGRAM --pair procid=$PID --pair unixtime=${UNIXTIME} --pair facility=$FACILITY --pair severity=$SEVERITY --pair timereported=${S_DATE} --pair pool_id=${.SDATA.kypo.pool_id} --pair sandbox_id=${.SDATA.kypo.sandbox_id} --pair message=$MESSAGE\n)}\n");
};

# BASH commands
filter f_bash_command { program("bash.command") };

destination d_man_bash_commands_logstash {
    network(
        "kypo-logstash"
        port(10515)
        transport(udp)
        ip-protocol(4)
        template(kypo_man_console_command_syslog_template)
    );
};

log {source(s_man); filter(f_bash_command); destination(d_man_bash_commands_logstash);};

# MSF4 commands
filter f_msf4_command { program("msf.command") };

destination d_man_msf_commands_logstash {
    network(
        "kypo-logstash"
        port(10516)
        transport(udp)
        ip-protocol(4)
        template(kypo_man_console_command_syslog_template)
    );
};

log {source(s_man); filter(f_msf4_command); destination(d_man_msf_commands_logstash);};
