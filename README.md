This project was moved to https://gitlab.ics.muni.cz/muni-kypo/ansible-roles/syslog-ng

# Ansible role - KYPO CRP syslog-ng

This role setups syslog-ng service for KYPO CRP.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role parameters

You can override default values of these parameters:

* `kypo_crp_config_dest` - Path, where all configuration will be created.
* `kypo_crp_syslog_ng_compose_filename` - Filename for docker-compose template.
* `kypo_crp_docker_services` - Dictionary with settings for Docker services.
* `kypo_crp_docker_network_name` - The name of the Docker network used by syslog-ng container. If not specified, default Docker network will be used.

(default: [see](defaults/main.yml))
